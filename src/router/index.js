import { getCookie } from '@/utils/cookie'
import { copyObj } from '@/utils/myFun'
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// 静态路由
const constRoutes = [
  { path: '/', redirect: '/login' },
  {
    path: '/Login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '@/views/Login')
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "NotFound" */ '@/views/NotFount')
  }
]

// 动态路由，部分路由从接口获取
export const asyncRoutes = [
  {
    path: '/Layout',
    component: () => import(/* webpackChunkName: "Layout" */ '@/views/Layout'),
    meta: {
      requiresAuth: true
    },
    children: [
      { path: '/', redirect: '/Home' },
      {
        path: '/Home',
        name: 'Home',
        component: () => import(/* webpackChunkName: "Home" */ '@/views/Home')
      },
      {
        path: '/Echarts/Bar',
        name: 'Bar',
        component: () => import(/* webpackChunkName: "Bar" */ '@/views/Echarts/Bar')
      },
      {
        path: '/Echarts/Line',
        name: 'Line',
        component: () => import(/* webpackChunkName: "Line" */ '@/views/Echarts/Line')
      },
      {
        path: '/Echarts/Pie',
        name: 'Pie',
        component: () => import(/* webpackChunkName: "Pie" */ '@/views/Echarts/Pie')
      },
      {
        path: '/Echarts/Map',
        name: 'Map',
        component: () => import(/* webpackChunkName: "Map" */ '@/views/Echarts/Map')
      },
      {
        path: '/Example/Tables',
        name: 'Tables',
        component: () => import(/* webpackChunkName: "Tables" */ '@/views/Example/Tables')
      },
      {
        path: '/Example/Clipboard',
        name: 'Clipboard',
        component: () => import(/* webpackChunkName: "Clipboard" */ '@/views/Example/Clipboard')
      },
      {
        path: '/Example/Moment',
        name: 'Moment',
        component: () => import(/* webpackChunkName: "Moment" */ '@/views/Example/Moment')
      },
      {
        path: '/Example/Tinymce',
        name: 'Tinymce',
        component: () => import(/* webpackChunkName: "Tinymce" */ '@/views/Example/Tinymce')
      },
      {
        path: '/Example/Backtop',
        name: 'Backtop',
        component: () => import(/* webpackChunkName: "Backtop" */ '@/views/Example/Backtop')
      },
      {
        path: '/Example/BaiduMap',
        name: 'BaiduMap',
        component: () => import(/* webpackChunkName: "BaiduMap" */ '@/views/Example/BaiduMap')
      },
      {
        path: '/Example/QRcode',
        name: 'QRcode',
        component: () => import(/* webpackChunkName: "QRcode" */ '@/views/Example/QRcode')
      },
      {
        path: '/Example/TreeMenu',
        name: 'TreeMenu',
        component: () => import(/* webpackChunkName: "TreeMenu" */ '@/views/Example/TreeMenu')
      },
      {
        path: '/SystemManagement/UserManagement',
        name: 'UserManagement',
        component: () => import(/* webpackChunkName: "UserManagement" */ '@/views/SystemManagement/UserManagement')
      },
      {
        path: '/SystemManagement/RoleManagement',
        name: 'RoleManagement',
        component: () => import(/* webpackChunkName: "RoleManagement" */ '@/views/SystemManagement/RoleManagement')
      },
      {
        path: '/SystemManagement/MenuManagement',
        name: 'MenuManagement',
        component: () => import(/* webpackChunkName: "MenuManagement" */ '@/views/SystemManagement/MenuManagement')
      },
      {
        path: '/SystemManagement/DictManagement',
        name: 'DictManagement',
        component: () => import(/* webpackChunkName: "DictManagement" */ '@/views/SystemManagement/DictManagement')
      },
      {
        path: '/CustomComponents/Button',
        name: 'Button',
        component: () => import(/* webpackChunkName: "Button" */ '@/views/CustomComponents/Button')
      },
      // 微前端应用统一挂载
      {
        path: '/Micro/*',
        name: 'Micro',
        component: () => import(/* webpackChunkName: "Micro" */ '@/views/Micro')
      }
    ]
  }
]

export const router = new VueRouter({
  mode: 'history',
  base: '/',
  // base: '/base/',
  routes: constRoutes
})

// 用户登录后第一次调用该方法
export const addMenuData = () => {
  if (getCookie('menuData')) {
    // 动态路由数据格式转换
    const tempArr = []
    JSON.parse(getCookie('menuData')).map((item) => {
      tempArr.push({
        path: item.path,
        name: item.name,
        component: () => import('@/views' + item.path)
      })
    })
    // 对象深拷贝，防止修改原数据
    const tempObj = copyObj(asyncRoutes)
    tempObj[0].children = asyncRoutes[0].children.concat(tempArr)

    // 路由重置
    const newRouter = new VueRouter({
      mode: 'history',
      base: '/',
      // base: '/base/',
      routes: constRoutes
    })
    router.matcher = newRouter.matcher
    // 添加动态路由
    router.addRoutes(tempObj)
  }
}

// 刷新页面需要重新获取动态路由
addMenuData()
