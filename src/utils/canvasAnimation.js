// 循环动画
export let animation = null

// 开始下落动画
export const animationStart = (canvas, context, animationName) => {
  class Snow {
    constructor(radius) {
      this.x = 0
      this.y = 0
      this.radius = radius
      this.color = ''
    }

    draw(context) {
      context.save()
      context.translate(this.x, this.y)
      context.fillStyle = this.color
      context.beginPath()
      // x, y, radius, start_angle, end_angle, anti-clockwise
      context.arc(0, 0, this.radius, 0, Math.PI * 2, true)
      context.closePath()
      context.fill()
      context.restore()
    }
  }

  class Rain {
    constructor(radius) {
      this.x = 0
      this.y = 0
      this.radius = radius
      this.color = ''
    }

    draw(context) {
      context.save()
      context.translate(this.x, this.y)
      context.fillStyle = this.color
      context.beginPath()
      context.moveTo(this.x, this.y) // 起点
      context.lineTo(this.x + 3, this.y + 15) // 绘制线条
      var radius = 3 // 圆形半径
      context.arc(this.x, this.y + 15, radius, 0, (180 * Math.PI) / 180) // 绘制一个半圆
      context.closePath() // 闭合路径
      context.fill() // 填充
      context.restore()
    }
  }

  function moveSnow(snow, index) {
    snow.y += snow.radius / 3
    if (snow.y > canvas.height) snows.splice(index, 1)
    else snow.draw(context)
  }

  let lastSnowCreatedTime = new Date()
  const snows = []
  // 根据时间创造新的雪花与气泡
  function createSnow() {
    const now = new Date()
    if (now - lastSnowCreatedTime > snows.length - now.getMinutes() && snows.length < 1000) {
      const radius = Math.random() * 5 + 2
      const snow = new Snow(radius)
      snow.x = Math.random() * canvas.width + 1
      snow.color = '#fff'
      snows.push(snow)
      lastSnowCreatedTime = now
    }
  }

  // 根据时间创造新的雪花与气泡
  function createRain() {
    const now = new Date()
    if (now - lastSnowCreatedTime > snows.length - now.getMinutes() && snows.length < 1000) {
      const radius = Math.random() * 5 + 2
      const snow = new Rain(radius)
      snow.x = Math.random() * canvas.width + 1
      snow.color = '#fff'
      snows.push(snow)
      lastSnowCreatedTime = now
    }
  }

  function drawFrame() {
    // 窗口resize时适配
    canvas.width = document.body.clientWidth
    canvas.height = document.body.clientHeight

    animation = requestAnimationFrame(drawFrame)
    context.clearRect(0, 0, canvas.width, canvas.height)
    if (animationName === 'snow') {
      createSnow()
    }
    if (animationName === 'rain') {
      createRain()
    }
    snows.forEach(moveSnow)
  }
  drawFrame()
}

// 停止下落动画
export const animationStop = (canvas, context) => {
  // 清空画布
  context.clearRect(0, 0, canvas.width, canvas.height)
  // 停止动画
  window.cancelAnimationFrame(animation)
  // 将动画置空
  animation = null
}
