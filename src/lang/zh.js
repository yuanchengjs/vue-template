export default {
  // 菜单静态路由
  Home: '首页',

  // echarts图表
  Echarts: '图表',
  Bar: '柱状图',
  Line: '折线图',
  Pie: '饼图',
  Map: '地图',

  // example常见功能
  Example: '常见功能',
  Tables: '表格',
  Clipboard: '剪贴板',
  Moment: '时间格式化',
  Tinymce: '富文本编辑器',
  Backtop: '返回顶部',
  BaiduMap: '百度地图',
  QRcode: '生成二维码',
  TreeMenu: '树形菜单',

  // 菜单动态路由
  DynamicMenu: '动态菜单',
  UserMenu: '普通角色菜单',
  AdminMenu: '管理员角色菜单',

  // 页面内容
  Search: '搜索',
  Reset: '重置',

  // npm组件
  CustomComponents: 'npm 组件',
  Button: '按钮',

  // 微前端
  Micro: '微前端',
  About: '关于',
  Vue2: 'Vue2',
  Vue3: 'Vue3',

  // 系统管理
  SystemManagement: '系统管理',
  UserManagement: '用户管理',
  RoleManagement: '角色管理',
  MenuManagement: '菜单管理',
  DictManagement: '字典管理'
}
