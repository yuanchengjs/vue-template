import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '共享变量名'
  },
  mutations: {
    changeName(state, newName) {
      state.name = newName
    }
  },
  actions: {},
  modules: {}
})
