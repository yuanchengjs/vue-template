## 1.启动项目

- 安装依赖：yarn
- 启动本地环境：yarn serve
- 打包测试环境：yarn qa
- 打包生产环境：yarn build

## 2.项目登录

- 用户名：admin
- 密码：123456

## 3.项目功能

- 多环境配置
- 国际化
- 动态路由
- 面包屑
- 标签导航
- 404 错误页面
- 富文本
- 弹出框可拖拽
- 按钮点击波纹动画
- 请求进度条
- 按钮防抖
- 剪切板文字复制
- 时间格式化
- 高度自适应
- 封装 axios 请求
- 表格 Excel 导出
- url 链接生成二维码
- canvas 转图片下载
- 百度地图
- Echarts 图表
- ESLint 代码检测
- gzip 压缩
- cdn 加速
- 音乐在线搜索播放
- npm 抽离公共组件方法和样式
- 接入 qiankun 微前端
- scss 全局变量配置
- canvas 实现下雨下雪动画
- 动态更换皮肤
- swagger 接口文档(云数据库已到期)

## 4.addRoutes 动态路由实现

- 登录成功后从后端获取动态路由菜单，并将其存入 cookie 中，同时调用 addMenuData 方法添加动态路由
- 在菜单配置页面需将菜单数据转成树状结构，然后通过递归组件生成多层级路由菜单
- 当刷新页面时动态路由会丢失，需在刷新页面时重新拉取动态路由菜单
- 动态路由添加时，router 需要先重置，避免控制台报路由重复警告
- 动态路由添加时，asyncRoutes 也需要被重置，可使用深拷贝避免其被修改

```js
// src/router/index.js
// 用户登录后第一次调用该方法
export const addMenuData = () => {
  if (getCookie('menuData')) {
    // 动态路由数据格式转换
    const tempArr = []
    JSON.parse(getCookie('menuData')).map((item) => {
      tempArr.push({
        path: item.path,
        name: item.name,
        component: () => import('@/views' + item.path)
      })
    })
    // 对象深拷贝，防止修改原数据
    const tempObj = copyObj(asyncRoutes)
    tempObj[0].children = asyncRoutes[0].children.concat(tempArr)

    // 路由重置
    const newRouter = new VueRouter({
      mode: 'history',
      base: '/',
      // base: '/base/',
      routes: constRoutes
    })
    router.matcher = newRouter.matcher
    // 添加动态路由
    router.addRoutes(tempObj)
  }
}

// 刷新页面需要重新获取动态路由
addMenuData()
```

## 5.双 token 校验

- 登录成功之后保存 refresh_token 和 access_token，以及它们的过期时间
- 在过期时间前一段时间，调用接口通过 refresh_token 重新获取 refresh_token 和 access_token

## 6.系统管理

- 角色管理：在该页面配置角色，不同的角色配置不同的菜单
- 用户管理：基于角色创建用户，可设置用户信息，使用用户名和密码登录，只可访问该角色下的菜单
- 菜单管理：在该页面配置所有的菜单，配置项包括菜单中文名称，菜单英文名称，菜单 url 路径，菜单图标，菜单父目录，菜单序号等
- 字典管理：配置键值对，适用于下拉框和表格显示

## 7.qiankun 微前端接入

- 后台管理项目过大会导致代码编译过慢，内存泄漏，不易管理维护等诸多问题
- router-view 外不得使用 transition 包裹，会导致 dom 未加载的报错
- 到域名解析中添加主机记录：vue2，vue3，react
- 微前端的子应用需要在 nginx 中设置域名允许被跨域
- 基座的注册和启动子应用不要放到 main.js 中，应该放到挂载页面的 mounted 生命周期中，确保 dom 已经加载
- 与 CDN 可能会冲突
- vue3 没有 observable 方法，可在 data 中声明变量来保存 this.\$microStore.state.name 的值
- 微应用中的图片资源无法被加载，按照官网解决未果，可将图片 url 前缀固定

## 8.接口介绍

- 用户登陆：输入用户名和密码即可登录获取 token，4h 后过期
- 用户注册：输入用户名和密码即可注册用户
- 用户列表：获取当前存在的用户，接口需要 token 校验
- 用户删除：删除当前用户，接口需要 token 校验
- 用户注册，用户列表，用户删除接口均在角色管理页面被调用
- 默认用户为 admin 不可删除
