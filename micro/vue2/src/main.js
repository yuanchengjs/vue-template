import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

if (process.env.NODE_ENV === 'development') {
  window.env = 'http://localhost:8080'
} else {
  window.env = 'https://vue2.yuanchengjs.cn'
}

Vue.config.productionTip = false

let instance = null
function render(props = {}) {
  const microStore = props.store

  // 绑定到window上
  window.$microStore = microStore

  // Vue.observable(microStore)
  // Vue.prototype.$microStore = microStore

  const { container } = props

  instance = new Vue({
    router,
    store,
    render: (h) => h(App)
  }).$mount(container ? container.querySelector('#app') : '#app')
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

export async function bootstrap() {
  console.log('subappVue2初始化')
}
export async function mount(props) {
  console.log('subappVue2完成加载', props)
  render(props)
}
export async function unmount() {
  console.log('subappVue2完成销毁')
  instance.$destroy()
  instance.$el.innerHTML = ''
  instance = null
}
